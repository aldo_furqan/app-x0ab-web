<?php
	$DB_NAME = "kampus";
	$DB_USER = "root";
	$DB_PASS = "";
	$DB_SERVER_LOC = "localhost";

	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
		$sql = "select m.nim, m.nama, p.nama_prodi, 
                concat('http://192.168.43.170/www/kampus/images/',photos) as url,
				m.alamat
				from mahasiswa m, prodi p where m.id_prodi = p.id_prodi";
		$result = mysqli_query($conn, $sql);
		$show = mysqli_num_rows($result);
		if( $show > 0){
			header("Access-Control-Allow-Origin: *");
			header("Content-type: application/json; charset=UTF-8");
			
			$data_mhs = array();
			while ($mhs = mysqli_fetch_assoc($result)) {
				array_push($data_mhs, $mhs);
			}
			echo json_encode($data_mhs);
		}
	}
?>